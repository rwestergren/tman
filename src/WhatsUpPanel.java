

import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;



public class WhatsUpPanel extends JPanel {
    Student student = null;
     public WhatsUpPanel(Student student) {
        super();
        this.student=student;
        GridLayout grid = new GridLayout(20, 1);
        setLayout(grid);
        for (int i = 0;i<20; i++){
          JButton jb1 = new JButton(student.whatsUp());
          add(jb1, BorderLayout.SOUTH);//seems to do nothing
        }
        setBackground(Color.gray);
     }
}
