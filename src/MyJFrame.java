
import java.awt.*;
import javax.swing.*;
import java.awt.BorderLayout;

public class MyJFrame extends JFrame {

    public MyJFrame() {
        super("My First Frame");
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

// Create components: Jpanel
        MyJPanel mjp = new MyJPanel();
        mjp.setBackground(Color.gray);
//------------------------------------------------------
        getContentPane().add(mjp, "Center");
//------------------------------------------------------
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600, 575);
        setVisible(true);
    }

}
