public class Student
{
        String firstName;
	String lastName;
	int age;
        int c = 0;
        int w = 0;
        int o = 0;
        int e = 0;
        
	Student(String informedFirstName, String informedLastName, int informedAge)
	{
		firstName = informedFirstName;
		lastName = informedLastName;
		age = informedAge;
	}
	
	String getName()
	{
		return firstName +" " + lastName;
                
	}
        String whatsUp()
     {
         //this randomly selects 1 of 4 activities, 20 times due to the loop in app class
        
         String activity = this.randomactivity();
         if ("doing java".equals(activity))//this tracks the activity
                 {
                     c +=1;
                 }
         if ("searching the web".equals(activity))//this tracks the activity
                 {
                     w +=1;
                 }
         if ("listening to endless lecture".equals(activity))//this tracks the activity
                 {
                     o +=1;
                 }
         if ("eating".equals(activity))//this tracks the activity
                 {
                     e +=1;
                 }
         String answer = "John is "+ activity;
        
         return answer;
         
     }

    private String randomactivity() {//this outputs the random activity
        double myRandomNumber = Math.random();
        double myRandomNumber_as_double = (myRandomNumber * 4);
        int myRandomActivityNum = (int) myRandomNumber_as_double;
        if  (myRandomActivityNum == 0)
        {
            return "doing java";
        }
        else if (myRandomActivityNum == 1)
        {
            return "searching the web";
        }
        else if (myRandomActivityNum == 2)
        {
            return "listening to endless lecture";
        }
        else if (myRandomActivityNum == 3)
        {
            return "eating";
        }
            return null;
    }
}
