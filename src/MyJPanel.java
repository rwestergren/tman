
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class MyJPanel extends JPanel {

    public MyJPanel() {
        
        super();
        //GridLayout grid = new GridLayout(2,1);        
         BoxLayout grid = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        
        setLayout(grid);
        //grid.setVgap(1);
        Student st1 = new Student("John", "Jones", 30);
        StudentPanel sp = new StudentPanel(st1);
        WhatsUpPanel wup = new WhatsUpPanel(st1);        
        add(sp);
        add(wup);

    }
}
